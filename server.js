const express = require('express');
const app = express();
const PORT = 5000;
const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

var bodyParser = require('body-parser')
 
// create application/json parser
var jsonParser = bodyParser.json()

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/drive'];

// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

let auth;

function authorize(token) {
  let rawdata = fs.readFileSync('credentials.json');
  let credentials = JSON.parse(rawdata);

    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id,
      client_secret,
      redirect_uris[0]
    );
      console.log("token:dd::::::", token);
      oAuth2Client.setCredentials(token);
      auth = oAuth2Client;
  }


  app.post('/uploadAFile',jsonParser,upload.single('img'),(req, res) => {
    console.log("REQUEST BODY::::", req.file);
    var folderId = req.body.folderId;
    var access_token = req.headers.access_token;
    var refresh_token = req.headers.refresh_token;
    var expiry_date = req.headers.expiry_date;

    let token = {
      "access_token":access_token,"refresh_token":refresh_token,"scope":"https://www.googleapis.com/auth/drive","token_type":"Bearer","expiry_date":expiry_date
    }

    if(!access_token || !refresh_token || !expiry_date)
    {
      return res
      .status(400)
      .json({ errors: [{ msg: 'token missing or incomplete, add missing keys in header(access_token, refresh_token, expiry_date)' }] });
    }
    authorize(token);
    var fileMetadata;
    if(!folderId){
      fileMetadata = {
        name: req.file.filename// file name that will be saved in google drive
      };
    }
    fileMetadata = {
      name: req.file.filename,
      parents: [folderId] // file name that will be saved in google drive
    };
    var media = {
      mimeType: req.file.mimetype,
      body: fs.createReadStream(req.file.path), // Reading the file from our server
    };
  
    // Authenticating drive API
    const drive = google.drive({ version: 'v3', auth });
    // Uploading Single image to drive
    drive.files.create(
      {
        resource: fileMetadata,
        media: media,
        
      },
      async (err, file) => {
        if (err) {
          // Handle error
          console.error(err.msg);
  
          return res
            .status(400)
            .json({ errors: [{ msg: 'Server Error try again later' }] });
        } else {
          // if file upload success then return the unique google drive id
          res.status(200).json({
            fileID: file.data.id,
          });
        }
      }
    );
  });

app.get('/testRoute', (req, res) => res.end('Hello from Server!'));

app.listen(PORT, () => {
  console.log(`Node.js App running on port ${PORT}...`);
});